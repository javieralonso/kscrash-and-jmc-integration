/*
 * Author: Javier Alonso <javieralonso@gmail.com>
 *
 * Copyright (c) 2013 Javier Alonso. All rights reserved.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
/*
 * Author: Andreas Linde <mail@andreaslinde.de>
 *         Kent Sutherland
 *
 * Copyright (c) 2009 Andreas Linde & Kent Sutherland. All rights reserved.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#import "CrashReporter.h"
#import "JMCMacros.h"
#import "KSCrashAdvanced.h"
#import "KSCrashReportFilterAppleFmt.h"

static CrashReporter *crashReportSender = nil;

@interface CrashReporter ()


@end


@implementation CrashReporter

+ (void)enableCrashReporter
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        crashReportSender = [[CrashReporter alloc] init];
    });
}

/**
* If Crash Reports are enabled, this will return the sharedCrashReporter instance.
* Else, it returns nil.
*/
+ (CrashReporter *)sharedCrashReporter
{
    return crashReportSender;
}

- (id)init
{
    self = [super init];

    if (self != nil)
    {

        NSString *testValue = [[NSUserDefaults standardUserDefaults] stringForKey:kCrashReportAnalyzerStarted];
        if (testValue == nil)
        {
            _crashReportAnalyzerStarted = 0;
        } else
        {
            _crashReportAnalyzerStarted = [[NSUserDefaults standardUserDefaults] integerForKey:kCrashReportAnalyzerStarted];
        }

        testValue = nil;
        testValue = [[NSUserDefaults standardUserDefaults] stringForKey:kCrashReportActivated];
        if (testValue == nil)
        {
            _crashReportActivated = YES;
            [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kCrashReportActivated];
        } else
        {
            _crashReportActivated = [[NSUserDefaults standardUserDefaults] boolForKey:kCrashReportActivated];
        }

        if (_crashReportActivated)
        {
            
            // Enable the Crash Reporter
            if (![KSCrash installWithCrashReportSink:nil]){
                JMCALog(@"Warning: Could not enable crash reporter");
                return nil;
            }

            JMCDLog(@"Crash reporter enabled.");
        }
    }
    return self;
}


#pragma mark -
#pragma mark Private

- (void)cleanCrashReports
{
    [[KSCrash instance] deleteAllReports];
}

- (NSArray *)crashReports
{
    KSCrashReportFilterAppleFmt *appleFormatFilter = [KSCrashReportFilterAppleFmt filterWithReportStyle:KSAppleReportStyleSymbolicated];
    __block NSArray *result=nil;
    
    /*
     WARNING: this code works since KSCrashReportFilterAppleFmt works synchronously. Using an asynchronous filter will result in undefined behaviour.
     To use async filters, make sure this method (crashReports) is not called on main thread and use GCD semaphores
     */
    [appleFormatFilter filterReports:[[KSCrash instance] allReports] onCompletion:^(NSArray *filteredReports, BOOL completed, NSError *error) {
        result=filteredReports;
    }];
    
    return result;
}

-(BOOL)hasPendingCrashReport{
    return [[KSCrash instance] reportCount]>0;
}


@end
